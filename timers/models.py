from django.db import models
from django.core.validators import RegexValidator, validate_email
from django.forms import ModelForm, Form
from django import forms
# Create your models here.
class Timer(models.Model):
	alias = models.CharField(max_length=200, validators=[RegexValidator(r'^[a-z0-9_-]{3,15}$', 'alias can only have Aa/1/-/_', 'Invalid Alias' )])
	title = models.CharField(max_length=140) 
	pub_date = models.DateTimeField('date published')
	target_date = models.DateTimeField('target date')
	# owner_email = models.CharField(max_length=200, validators=[validate_email])


class TimerForm(ModelForm):
	class Meta:
		model = Timer
		exclude = ['pub_date','alias']


class CountdownForm(forms.Form):
	title = forms.CharField(max_length=50)
	# day = forms.ChoiceField(choices=[("DD","DD")] + [(x, x) for x in range(1, 32)])
	# month = forms.ChoiceField(choices=[("DD","MM")] + [(x, x) for x in range(1, 13)])
	# year = forms.ChoiceField(choices=[("DD","YYYY")] + [(x, x) for x in range(2014, 2099)])
	hour = forms.CharField(max_length=2,  widget=forms.TextInput(attrs={'size':'1'}))
	minute = forms.CharField(max_length=2, widget=forms.TextInput(attrs={'size':'1'}))
	day = forms.CharField(max_length=2,  widget=forms.TextInput(attrs={'size':'1'}))
	month = forms.CharField(max_length=2,  widget=forms.TextInput(attrs={'size':'1'}))
	year = forms.CharField(max_length=4,  widget=forms.TextInput(attrs={'size':'3'}))