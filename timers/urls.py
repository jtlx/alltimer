from django.conf.urls import patterns, include, url
from django.contrib import admin
from timers import views

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^(?P<timer_id>.+)/$', views.timer, name='timer'),
)