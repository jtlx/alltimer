from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from timers.models import Timer, TimerForm, CountdownForm
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib import messages 

# Create your views here.
def index(request):
	latest_timer_list = Timer.objects.order_by('-pub_date')
	template = loader.get_template('timers/index.html')

	context = RequestContext(request, {
		'latest_timer_list': latest_timer_list,
		'TimerForm' : TimerForm,
		'CountdownForm' : CountdownForm
	})
	return render(request, 'timers/index.html', context)

def timer(request, timer_id):
	timer = get_object_or_404(Timer, alias=timer_id)
	return render(request, 'timers/detail.html', {'timer': timer})


def createtimer(request):
	
	title = request.POST['title']
	day = request.POST['day']
	month = request.POST['month']
	year = request.POST['year']
	hour = request.POST['hour']
	minute = request.POST['minute']
	# target_date = request.POST['target_date']

	target_date = year + '-' + month + '-' + day + ' ' + hour + ':' + minute


	if title in [None, '']:
		messages.error(request, 'Enter a title.')
		return HttpResponseRedirect(reverse('index'))


	alias = title.replace (" ", "-")
	alias2 = alias

	x = 0

	while Timer.objects.filter(alias=alias2).exists():
		x += 1
		alias2 = alias + '-' + str(x)

	t = Timer(alias=alias2, title=title,  target_date=target_date, pub_date=timezone.now())
	try:
		t.save()
		return HttpResponseRedirect(reverse('timers:timer', args=(t.alias,)))
	except ValidationError:
		messages.error(request, 'Error with date & time.')
		return HttpResponseRedirect(reverse('index'))