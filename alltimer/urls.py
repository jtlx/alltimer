from django.conf.urls import patterns, include, url
from timers import views
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^admin/', include(admin.site.urls)),
	url(r'^createtimer/$', views.createtimer, name='createtimer'),
	url(r'^', include('timers.urls', namespace="timers")),
	
	# Examples:
	# url(r'^$', 'alltimer.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),

	
)
